Attribute VB_Name = "Win32API_me"
Option Explicit

Public Declare Sub mouse_event Lib "user32" ( _
                    ByVal dwFlags As Long, _
                    ByVal dx As Long, _
                    ByVal dy As Long, _
                    ByVal cButtons As Long, _
                    ByVal dwExtraInfo As Long)

Public Const MOUSEEVENTF_MOVE = &H1
Public Const MOUSEEVENTF_LEFTDOWN = &H2
Public Const MOUSEEVENTF_LEFTUP = &H4
Public Const MOUSEEVENTF_RIGHTDOWN = &H8
Public Const MOUSEEVENTF_RIGHTUP = &H10
Public Const MOUSEEVENTF_MIDDLEDOWN = &H20
Public Const MOUSEEVENTF_MIDDLEUP = &H40
Public Const MOUSEEVENTF_ABSOLUTE = &H8000

