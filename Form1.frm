VERSION 5.00
Begin VB.Form MainForm 
   BorderStyle     =   1  '固定(実線)
   Caption         =   "Click The Left"
   ClientHeight    =   810
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   2235
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   54
   ScaleMode       =   3  'ﾋﾟｸｾﾙ
   ScaleWidth      =   149
   StartUpPosition =   2  '画面の中央
   Begin VB.Timer Timer3 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   2160
      Top             =   480
   End
   Begin VB.Timer Timer2 
      Enabled         =   0   'False
      Left            =   540
      Top             =   870
   End
   Begin VB.TextBox Text1 
      Height          =   300
      IMEMode         =   3  'ｵﾌ固定
      Left            =   825
      MaxLength       =   5
      TabIndex        =   2
      Text            =   "3000"
      Top             =   75
      Width           =   750
   End
   Begin VB.CommandButton Command2 
      Caption         =   "STOP"
      Enabled         =   0   'False
      Height          =   300
      Left            =   1125
      TabIndex        =   1
      Top             =   450
      Width           =   900
   End
   Begin VB.CommandButton Command1 
      Caption         =   "START"
      Height          =   300
      Left            =   150
      TabIndex        =   0
      Top             =   450
      Width           =   900
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   60
      Top             =   870
   End
   Begin VB.Label Label2 
      Caption         =   "msec"
      Height          =   225
      Left            =   1650
      TabIndex        =   4
      Top             =   105
      Width           =   450
   End
   Begin VB.Label Label1 
      Caption         =   "Interval"
      Height          =   225
      Left            =   150
      TabIndex        =   3
      Top             =   75
      Width           =   600
   End
End
Attribute VB_Name = "MainForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const STOPTIME As Long = 3& * 60& * 60&

Dim c As Long
Dim t As Long
Dim s As Long

Private Sub Command1_Click()
    t = 5
    c = 0
    s = 0
    Text1.Text = Format$(CLng(Val(Text1.Text)))
    If Val(Text1.Text) < 10 Then
        MsgBox "10msec以上にする必要があります！", vbExclamation, "入力エラー"
        Exit Sub
    End If
    Timer2.Interval = CLng(Val(Text1.Text))
    Timer1.Enabled = True
    Command1.Enabled = False
    Command2.Enabled = True
    Text1.Enabled = False
End Sub

Private Sub Command2_Click()
    Timer2.Enabled = False
    Timer1.Enabled = False
    Timer3.Enabled = False
    Command2.Enabled = False
    Command1.Enabled = True
    Text1.Enabled = True
End Sub

Private Sub Timer1_Timer()
    MainForm.Caption = "Count " + Str$(t)
    If t > 0 Then
        t = t - 1
    Else
        Timer1.Enabled = False
        Timer2.Enabled = True
        Timer3.Enabled = True
    End If
End Sub

Private Sub Timer2_Timer()
    On Error GoTo Error_Label
    c = c + 1
    MainForm.Caption = "Click =" + Str$(c)
    mouse_event MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0
    mouse_event MOUSEEVENTF_LEFTUP, 0, 0, 0, 0
    Exit Sub
Error_Label:
    Timer2.Enabled = False
    Timer3.Enabled = False
    MsgBox Err.Description & "[" & CStr(Err.Number) & "]", vbCritical, "エラー"
    Err.Clear
    Call Command2_Click
End Sub

Private Sub Timer3_Timer()
    s = s + 1
    If s < STOPTIME Then
        Exit Sub
    End If
    Timer3.Enabled = False
    Timer2.Enabled = False
    MsgBox "長時間動作に対して自動停止しました", vbInformation, "強制停止"
    Call Command2_Click
End Sub
